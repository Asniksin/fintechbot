﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace fintech.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var message = await result as IMessageActivity;

            bool isRegisterDone = false; //There should be registration by default

            var userData = context.UserData;

           if (userData != null)
                userData.TryGetValue<bool>("IsRegisterDone", out isRegisterDone);

            if (!isRegisterDone)
                context.Call<object>(new RegisterDialog(), MessageReceivedAsync);
            else
                context.Call<object>(new MainDialog(), MessageReceivedAsync);
            
        }
    }
}