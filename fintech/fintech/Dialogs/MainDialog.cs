﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace fintech.Dialogs
{
    [Serializable]
    public class MainDialog : IDialog<object>
    {
        const int MAX_MONEY = 6000000;  //Amount of money needed to win

        String token;
        String game_id;
        String username;

        ResponseOpp resp;

        public async Task StartAsync(IDialogContext context)
        {
            if (!getData(context))
                return;
            
            if (!getOpportunity())
                return;

            await sendOpportunuty(context);
            context.Wait(sendAnswer);
        }

        private async Task sendOpportunuty(IDialogContext context)
        {
            if (IsItLose())
            {
                await context.PostAsync("К сожалению, деньги на вашем счете закончились. Попробуйте еще раз!");

                var userData = context.UserData;
                userData.SetValue("IsRegisterDone", false);

                context.Done<object>(new object());

                return;
            }

            if (IsItWin())
            {
                await context.PostAsync("Поздравляем, вы победили на " + resp.game.step 
                    + "-м ходу! Теперь вы знаете, как правильно поступать с деньгами.");

                var userData = context.UserData;
                userData.SetValue("IsRegisterDone", false);

                context.Done<object>(new object());

                return;
            }

            var optionsMessage = context.MakeMessage();

            optionsMessage.Attachments = new List<Attachment>();
            List<CardAction> cardButtons = new List<CardAction>();

            //creating a list of available options
            List<Opportunity.option> availableOptions = resp.opportunity.options.FindAll(x => x.cost <= resp.game.money);
            if (availableOptions.Count == 0)
                availableOptions = resp.opportunity.options;

            foreach (Opportunity.option op in availableOptions)
            {
                CardAction plButton = new CardAction()
                {
                    Value = (resp.opportunity.options.IndexOf(op) + 1).ToString(),
                    Type = "postBack",
                    Title = op.label
                };
                cardButtons.Add(plButton);
            }

            SigninCard plCard = new SigninCard(getCardText(), cardButtons);
            Attachment plAttachment = plCard.ToAttachment();
            optionsMessage.Attachments.Add(plAttachment);

            await context.PostAsync(optionsMessage);
        }

        private async Task sendAnswer(IDialogContext context, IAwaitable<object> result)
        {
            IMessageActivity answer = await result as IMessageActivity;

            int optionId = 0;

            //if user send a message without an option number
            if (!(int.TryParse(answer.Text, out optionId) && optionId > 0 && optionId <= resp.opportunity.options.Count))
            {
                await sendOpportunuty(context);
                return;
            }

            String requestText = "username=" + username + "&token=" + token + "&game_id=" + game_id + "&op_id=" + resp.opportunity.id + "&op_option_id=" + (optionId - 1);

            //send request and get response
            String responseText = getResponse("http://188.166.160.242:8080/apply", requestText);

            JToken jtoken = JToken.Parse(responseText);
            if (jtoken is JObject && ((JObject)jtoken)["message"] != null)
            {
                await context.PostAsync(((JObject)jtoken)["message"].ToString());
            }

            if (!getOpportunity())
                return;

            await sendOpportunuty(context);
        }

        private String getResponse(String url, String requestText)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(requestText);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse response = request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            string responseText;
            using (StreamReader reader = new StreamReader(responseStream))
            {
                responseText = reader.ReadToEnd();
            }
            responseStream.Close();

            return responseText;
        }

        private bool getData(IDialogContext context)
        {
            try {
                var userData = context.UserData;
                userData.TryGetValue<String>("Token", out token);
                userData.TryGetValue<String>("Game_ID", out game_id);
                userData.TryGetValue<String>("Username", out username);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool getOpportunity()
        {
            String text;
            try {
                WebRequest webrequest = WebRequest.Create("http://188.166.160.242:8080/next_move?username=" + username + "&token=" + token + "&game_id=" + game_id);
                WebResponse response = webrequest.GetResponse();
                Stream datastream = response.GetResponseStream();
                using (StreamReader reader = new StreamReader(datastream))
                {
                    text = reader.ReadToEnd();
                }
                datastream.Close();
            }
            catch
            {
                return false;
            }

            try {
                JavaScriptSerializer js = new JavaScriptSerializer();
                resp = js.Deserialize<ResponseOpp>(text);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private bool IsItLose()
        {
            return resp.game.money < 0;
        }

        private bool IsItWin()
        {
            return resp.game.money >= MAX_MONEY;
        }

        private String getCardText()
        {
            String text = Emoji.Clock + (resp.game.step + 1) + " | ";
            text += Emoji.MoneyBag + resp.game.money + " | ";
            text += Emoji.TriangleUp + resp.game.income + " | ";
            text += Emoji.TriangleDown + resp.game.expense + "\n\n";

            text += resp.opportunity.label + Environment.NewLine;
            return text;
        }
    }
}