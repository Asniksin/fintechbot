﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fintech.Dialogs
{
    public class Emoji
    {
        public static string MoneyBag { get { return "\U0001F4B0"; } }
        public static string Clock { get { return "\U0001F551"; } }
        public static string TriangleUp { get { return "\U0001F53A"; } }
        public static string TriangleDown { get { return "\U0001F53B"; } }
    }
}