﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fintech.Dialogs
{
    [Serializable]
    class ResponseOpp
    {
        public Opportunity opportunity { get; set; }
        public Game game { get; set; }
    }

    [Serializable]
    class Opportunity
    {
        public String id { get; set; }
        public String label { get; set; }
        public List<option> options { get; set; }

        [Serializable]
        public class option
        {
            public String label { get; set; }
            public int income { get; set; }
            public int cost { get; set; }
        }
    }

    [Serializable]
    class Game
    {
        public int step { get; set; }
        public int money { get; set; }
        public int income { get; set; }
        public int expense { get; set; }
    }
}