﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Text;

namespace fintech.Dialogs
{
    [Serializable]
    public class RegisterDialog : IDialog<object>
    {
        Job[] jobsList;  //All jobs available

        public async Task StartAsync(IDialogContext context)
        {
            await showLegend(context);

            await askJob(context);

            return;
        }

        private async Task askJob(IDialogContext context)
        {
            await showJobs(context);

            context.Wait(getAnswer);
        }

        private async Task showLegend(IDialogContext context)
        {
            String legend = "";
            legend += Emoji.Clock + " - Номер хода" + "\n\n";
            legend += Emoji.MoneyBag + " - Ваш баланс" + "\n\n";
            legend += Emoji.TriangleUp + " - Ваш доход" + "\n\n";
            legend += Emoji.TriangleDown + " - Ваши расходы" + "\n\n";

            await context.PostAsync(legend);
        }

        private async Task showJobs(IDialogContext context)
        {
            jobsList = getJobs();

            var jobListMessage = context.MakeMessage();

            String messageText = "";

            jobListMessage.Attachments = new List<Attachment>();
            List<CardAction> cardButtons = new List<CardAction>();

            foreach (Job job in jobsList)
            {
                messageText += job.label + " - Зарплата: " + job.salary + "  Квартплата: " + job.rent + "\n\n";

                CardAction plButton = new CardAction()
                {
                    Value = job.id,
                    Type = "postBack",
                    Title = job.label
                };
                cardButtons.Add(plButton);
            }

            SigninCard plCard = new SigninCard(messageText, cardButtons);
            Attachment plAttachment = plCard.ToAttachment();
            jobListMessage.Attachments.Add(plAttachment);

            await context.PostAsync(jobListMessage);
        }

        private async Task getAnswer(IDialogContext context, IAwaitable<object> result)
        {
            var answerMessage = await result as IMessageActivity;
            var activity = await result as Activity;

            String username = activity.ChannelId;
            String message = "username=" + username + "&job_id=" + answerMessage.Text;

            String responseText = PostRequest("http://188.166.160.242:8080/register", message);

            saveUserData(responseText, username, context);

            context.Done<object>(new object());
        }

        private void saveUserData(String response, String username, IDialogContext context)
        {
            JToken jtoken = JToken.Parse(response);
            if (jtoken is JObject && ((JObject)jtoken)["token"] != null && ((JObject)jtoken)["game_id"] != null)
            {
                String token = (String)((JObject)jtoken)["token"];
                String game_id = (String)((JObject)jtoken)["game_id"];

                var userData = context.UserData;
                userData.SetValue<String>("Token", token);
                userData.SetValue<String>("Game_ID", game_id);
                userData.SetValue<String>("Username", username);
                userData.SetValue<bool>("IsRegisterDone", true);
            }
        }

        private Job[] getJobs()
        {
            String jobsJson = GetRequest("http://188.166.160.242:8080/jobs");

            JavaScriptSerializer js = new JavaScriptSerializer();
            Job[] jobList = js.Deserialize<Job[]>(jobsJson);

            return jobList;
        }

        private String PostRequest(String url, String requestText)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(requestText);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse response = request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            string responseText;
            using (StreamReader reader = new StreamReader(responseStream))
            {
                responseText = reader.ReadToEnd();
            }
            responseStream.Close();

            return responseText;
        }

        private String GetRequest(String url)
        {
            WebRequest webrequest = WebRequest.Create(url);
            WebResponse response = webrequest.GetResponse();
            Stream datastream = response.GetResponseStream();
            string responseText;
            using (StreamReader reader = new StreamReader(datastream))
            {
                responseText = reader.ReadToEnd();
            }
            datastream.Close();

            return responseText;
        }

        [Serializable]
        class Job
        {
            public String id;
            public String label;
            public String story;
            public String emoji;
            public int salary;
            public int rent;
            public int init_money;
        }
    }
}